#!/bin/bash
#shellcheck disable=SC1090

. "$TF_DIR/include/common.sh"

tf_enum_subtests() {
    #
    # Stub: enumerate subtests
    #
    tf_warn "implement tf_enum_subtests()!"
    return "$TF_ES_ERROR"
}

tf_do_subtest() {
    #
    # Stub: perform test named $1
    #
    tf_warn "implement tf_do_subtest()!"
    return "$TF_ES_ERROR"
}

_tf_do_subtest() {
    #
    # Run single subtest inc. setup/cleanup if present
    #
    local subtname="$1"     # this subtest name
    local ses=0             # subtest exit status
    local setup=true        # setup command
    local cleanup=true      # cleanup command
    if test -f TF_SETUP;
    then
        setup=". TF_SETUP"
        bash -n TF_SETUP || {
            tf_warn "synax errors in TF_SETUP, skipping"
            return "$TF_ES_ERROR"
        }
    fi
    if test -f TF_CLEANUP;
    then
        setup=". TF_CLEANUP"
        bash -n TF_CLEANUP || {
            tf_warn "synax errors in TF_CLEANUP, skipping"
            return "$TF_ES_ERROR"
        }
    fi
    if $setup;
    then
        tf_do_subtest "$subtname"; ses=$?
    else
        tf_warn "setup phase failed, skipping: $subtname"
        ses=$TF_ES_ERROR
    fi
    if ! $cleanup;
    then
        tf_warn "cleanup phase failed: $subtname"
        ses=$TF_ES_PANIC
    fi
    return "$ses"
}

tf_do_subtests() {
    #
    # Run all subtests and return highest status
    #
    local es=0              # final exit status ("worst" of subtests)
    local subtname=""       # one subtest name
    local tes=""            # one subtest exit status
    local enumd=TF_ENUMERATED_SUBTESTS
    local fltrd=TF_FILTERED_SUBTESTS
    tf_enum_subtests >$enumd    || { tf_warn "error enumerating subtests"; return "$TF_ES_BAILOUT"; }
    test -s $enumd              || { tf_warn "no subtests enumerated";     return "$TF_ES_BAILOUT"; }
    grep -e "$TF_FILTER_SUBTEST" $enumd > $fltrd
    test -s $fltrd  || tf_debug "TF_FILTER_SUBTEST ate everything: $TF_FILTER_SUBTEST"

    for subtname in $(<$fltrd);
    do
        tf_think "::: $TF_TEST::$subtname"
        TF_SUBTEST=$subtname _tf_do_subtest "$subtname";
        tes=$?
        test $tes -gt $es              && es=$tes
        test $tes -gt "$TF_ES_OK"      && tf_warn "!!! $TF_TEST::$subtname ($tes)"
        test $tes -gt "$TF_ES_BAILOUT" && break
    done
    return $es
}
