#!/bin/bash
#shellcheck disable=SC1090
# tfkit - Shellfu's movable test framework
# See LICENSE file for copyright and license details.

#shellcheck disable=SC2153
. "$TF_DIR/include/common.sh"


__tf_collect_if_needed() {
    #
    # Collect relic if exit status suggests it
    #
    # Use test exit status $1 to help decide if relics are
    # needed, and collect them if so.
    #
    # If TF_COLLECT is set to "always", collect regardless of
    # the status.  If set to "never", do not collect anything.
    # The default setting, "auto" collects unless status is 0
    # or 1 (pass or bailout); in that case do nothing.
    #
    local tes=$1    # test exit status
    local will      # should we collect relics?
    case "$TF_COLLECT:$tes" in
        always:*)   will=true ;;
        never:*)    will=false ;;
        auto:0)     will=false ;;
        auto:2)     will=false ;;
        auto:*)     will=true ;;
        *)          tf_exit_bailout "bad value of TF_COLLECT: $TF_COLLECT" ;;
    esac
    $will || return 0
    mkdir -p "$relic_dir/$stamp"
    cp -r "$tmpdir"/* "$relic_dir/$stamp"
}

__tf_header() {
    #
    # create header to add to output before test
    #
    local hdrline                       # each line from generator
    local hdrgen="$TF_SUITE/TF_HEADER"  # header generator script
    tf_think "#"
    tf_think "# ---"
    tf_think "# tfkit:"
    if test -x "$hdrgen";
    then
        tf_think "#     sut:"
        $hdrgen \
          | while IFS= read -r hdrline;
            do
                test -n "$hdrline" || break
                tf_think "#         $hdrline"
            done
    else
        tf_think "#     hint: >"
        tf_think "#         Add $hdrgen executable for own header."
        tf_think "#         It should output YAML (\"key: value\" pairs) about"
        tf_think "#         your SUT ('version' at least')."
    fi
    tf_think "#     run:"
    tf_think "#         start_time: $(date -Iseconds)"
    test -n "$TF_FILTER_TEST" \
     && tf_think "#         TF_FILTER_TEST: $TF_FILTER_TEST"
    test -n "$TF_FILTER_SUBTEST" \
     && tf_think "#         TF_FILTER_SUBTEST: $TF_FILTER_SUBTEST"
    tf_think "#"
    tf_think ""
}

tf_enum_tests() {
    #
    # List what looks like test; relative to $TF_SUITE
    #
    tf_debug "TF_SUITE='$TF_SUITE'"
    test -d "$TF_SUITE" || return 0
    find -L \
        "$TF_SUITE" \
        -mindepth 2 \
        -maxdepth 2 \
        -type f \
        -perm /111 \
        -name TF_RUN \
      | cut -d/ -f2
}

tf_run_tests() {
    #
    # Discover and run tests
    #
    local es=0          # overall exit status
    local tmpdir=""     # test temporary dir
    local tname=""      # test name
    local tes=0         # test result
    local stamp=""      # test stamp to use as relic name
    local tf_dir        # to keep absolute path for TF_RUN
    local relic_dir     # where to keep relics
    relic_dir="$(readlink -f "$TF_RELICS")"
    __tf_header
    tf_debug "TF_VERSION='$TF_VERSION'"
    tf_dir="$(readlink -f "$TF_DIR")"
    es=0
    for tname in $(tf_enum_tests | grep -e "$TF_FILTER_TEST");
    do
        tf_think "... $tname"
        tmpdir=$(mktemp -d)
        stamp=$(date "+relics-$tname-%Y%m%d-%H%M%S")
        cp -r "$TF_SUITE/$tname/"* "$tmpdir"
        pushd "$tmpdir" >/dev/null || tf_exit_panic "could not chdir to: $tmpdir"
            TF_DEBUG=$TF_DEBUG TF_VERBOSE=$TF_VERBOSE \
            TF_DIR="$tf_dir" TF_TEST="$tname" \
            TF_FILTER_SUBTEST=$TF_FILTER_SUBTEST \
                ./TF_RUN
            tes=$?
            __tf_collect_if_needed $tes
            test $tes -gt $es && es=$tes
        popd >/dev/null || tf_exit_panic "could not chdir from: $tmpdir to $OLDPWD"
        rm -rf "$tmpdir"
        if test $tes -eq 0;
        then
            tf_think "''' $tname ($tes)"
        else
            tf_warn "??? $tname ($tes)"
        fi
    done
    return $es
}
